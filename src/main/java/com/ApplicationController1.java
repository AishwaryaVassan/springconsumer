package com;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
@Controller
public class ApplicationController1 {
@Autowired
DiscoveryClient ds;
 @RequestMapping("/")
 public void getEmployeeDetails() {
	// String url = "http://localhost:8331/employee";
	 //ResponseEntity<String> response = null;
    	 List<ServiceInstance> ls=ds.getInstances("Employee_Producer");
    	 ServiceInstance instance=ls.get(0);
    	 String url=instance.getUri().toString()+"/employee";
    	 RestTemplate restTemplate = new RestTemplate();
    	 ResponseEntity<String> response=null;
         try
         {
        	 response=restTemplate.exchange(url,HttpMethod.GET, getHeader(), String.class);
         }catch(Exception ex)
         {
       System.out.println(ex);
       
         }}
    	 
    	 /* response = restTemplate.exchange(url, HttpMethod.GET, getHeader(), String.class);

            System.out.println(response.getBody());

      }*/

 

      public static HttpEntity<?> getHeader()

      {    HttpHeaders header=new HttpHeaders();

      header.set("accept",MediaType.APPLICATION_JSON_VALUE);

      return new HttpEntity<>(header);

      }

}

 